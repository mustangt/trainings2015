#include <iostream>

int
main() 
{
    int r;
    
    std::cout << "Enter the radius of the circle: ";
    std::cin >> r;

    std::cout << "The diameter of the circle " << 2 * r << std::endl;

    std::cout << "The lenght of the circle " << 2 * 3.14159 * r << std::endl;

    std::cout << "The area of the circle " << 2 * 3.14159 * r * r << std::endl;
  
    return 0;
}
