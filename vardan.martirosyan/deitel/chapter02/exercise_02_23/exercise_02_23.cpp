#include <iostream> 
 
int 
main()
{
    int number1, number2, number3, number4, number5, min, max; 
 
    std::cout << "number1: " << std::endl; 
    std::cin >> number1; 
 
    std::cout << "number2: " << std::endl;
    std::cin >>  number2; 
 
    std::cout << "number3: " << std::endl; 
    std::cin >>  number3; 

    std::cout << "number4: " << std::endl;
    std::cin >> number4;
    
    std::cout << "number5: " << std::endl;
    std::cin >> number5;
       
    min = number1;
    if (min > number2) {
        min = number2;
    }
    if (min > number3) {
        min = number3;
    }
    if (min > number4) {
        min = number4;
    }
    if (min > number5) {
        min = number5;
    }
    max = number1;
    if (max < number2) {
        max = number2;
    }
    if (max < number3) {
        max = number3;
    }
    if (max < number4) {
        max = number4;
    }
    if (max < number5) {
        max = number5;
    }
    std::cout << "The min.value is " << min << std::endl;
    std::cout << "The max.value is " << max << std::endl;

    return 0;
}
