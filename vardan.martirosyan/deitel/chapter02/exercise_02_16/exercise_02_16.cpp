#include <iostream>

int 
main()
{
    int number1, number2, add, sub, mul, div;

    std::cout << "Input two numbers: ";
    std::cin  >> number1 >> number2;

    add = number1 + number2;
    std::cout << "number1 + number2 = " << add << std::endl;
	
    sub = number1 - number2;
    std::cout << "number1 - number2 = " << sub << std::endl;

    mul = number1 * number2;
    std::cout << "number1 * number2 = " << mul << std::endl;

    if (number2 != 0) {
        div = number1 / number2;
        std::cout << "number1 / number2 = " << div << std::endl;
    }
    if (number2 == 0) {
        std::cout << "Error number1 / number2 " << std::endl;
    }
    return 0;
}
