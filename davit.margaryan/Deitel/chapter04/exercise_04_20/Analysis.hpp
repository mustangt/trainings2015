#ifndef __ANALYSIS_HPP__
#define __ANALYSIS_HPP__

/// Analysis.hpp
/// Defintion of Analysis class, analysing the results of the exam

/// defintion of Analysis class
class Analysis
{
public:
    void processExamResults(); /// process the results of 10 students
}; /// end class Analysis

#endif /// __ANALYSIS_HPP__ 
