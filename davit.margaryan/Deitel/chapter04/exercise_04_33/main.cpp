///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    /// initialization of sides
    int side1 = 0, side2 = 0, 
        side3 = 0;
    /// prompt user to endter sides
    std::cout << "Enter the sides of right triangle: ";
    std::cin >> side1 >> side2 >> side3;
    /// error handling
    if (side1 <= 0) {
        std::cerr << "Error 1: Invlid side.\n";
        return 1;
    }
    if (side2 <= 0) {
        std::cerr << "Error 2: Invlid side.\n";
        return 2;
    }
    if (side3 <= 0) {
        std::cerr << "Error 3: Invlid side.\n";
        return 3;
    }
    side1 *= side1;
    side2 *= side2;
    side3 *= side3;

    if (side1 == side2 + side3) {
        std::cout << "Right triangle.\n";
    } else  if (side2 == side1 + side3) {
        std::cout << "Right triangle.\n";
    } else if (side3 == side1 + side2) {
        std::cout << "Right triangle.\n";
    } else {
        std::cout << "Not right triangle.\n";
    }

    return 0; /// program ends successfully
} /// end function main
