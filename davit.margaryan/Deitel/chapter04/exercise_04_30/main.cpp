///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    int radius = 0;

    /// prompt user to input radius
    std::cout << "Enter radius: ";
    std::cin >> radius;

    /// if radius is invalid
    if (radius < 0) {
        std::cout << "Error 1: Invalid radius.\n";
        return 1;
    } /// end while

    int diameter = 2 * radius;
    double pi = 3.14159;
    double circumference = diameter * pi;
    double area = radius * radius * pi;

    std::cout << "Diameter: " << diameter
              << "\nCircumference: " << std::fixed << circumference
              << "\nArea: " << std::fixed << area << std::endl;
    return 0; /// program ends successfully
} /// end function main
