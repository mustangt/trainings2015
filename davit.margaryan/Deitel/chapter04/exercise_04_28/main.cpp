/// output chess board

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    int row = 1;
    while (row <= 8) {
        if (row % 2 == 0) {
            std::cout << ' ';
        }
        int column = 1;
        while (column <= 8) {
            std::cout << "* ";
            column++;
        } /// end while
        std::cout << std::endl;
        ++row;
    }
    return 0; /// program ends successfully
} /// end function main
