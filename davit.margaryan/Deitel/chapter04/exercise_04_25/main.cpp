/// The program drow rectangle

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int size = 0;
    /// prompt user to enter size
    std::cout << "Enter size (1 - 20): ";
    std::cin >> size; /// imput size
    if (size < 1) { /// if size out of range
        std::cout << "Error - Out of range!\n";
        return 1; /// program ends with error
    } else if (size > 20) { /// else if size out of range
        std::cout << "Error - Out of range!\n";
        return 1; /// program ends with error
    } /// end else if

    /// intitializtion of column
    int column = 1;
    /// output rectangle
    while (column <= size) {
        int row = 1;
        while (row <= size) {
            if (1 == column) {
                std::cout << "*";
            } else if (1 == row) {
                std::cout << "*";
            } else if (row == size) {
                std::cout << "*";
            } else if (column == size) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++row;
        }
        std::cout << std::endl;
        ++column;
    } /// end while

    return 0; /// program ends successfully
} /// end function main
