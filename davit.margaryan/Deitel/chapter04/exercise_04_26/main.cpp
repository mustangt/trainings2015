/// main.cpp
/// The program detects if number is a palindrom or not

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialize data
    int number = 0;
    /// prompt user to enter number
    std::cout << "Enter number (10000 - 99999): ";
    std::cin >> number; /// input number

    if (number < 10000) { /// if number is out of range
        std::cout << "Error 1: number is out of range!\n";
        return 1; /// program ends with error
    } else if (number > 99999) { /// else if number is out of range
        std::cout << "Error 1: number is out of range!\n";
        return 1; /// program ends with error
    } /// end else if

    int first = 1; /// 10 pow first character index
    int last = 10000; /// 10 pow last character index

    /// while first number index is not equal to middle number index
    while (first < last && (number / first % 10 == number / last % 10)) {
        first *= 10;
        last /= 10;
    } /// end while

    /// if not end and inputs a valid number
    if(last == first) { /// if is palindrom
        std::cout << "The number is palindrom.\n"; /// display message
    } else { /// else
        std::cout << "The number is not palindrom.\n"; /// display message
    } /// end else

    return 0; /// program ends successfully
} /// end function main
