///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    int result = 1;
    while(true) {
        result *= 2;
        std::cout << result << std::endl;
    }
    return 0; /// program ends successfully
} /// end function main
