/// The program converts binary number to decimal

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program ebinaryecution
int
main()
{
    /// intialization of data
    int binary = 0; 

    /// prompt user to enter binary number
    std::cout << "Enert binary number: ";
    std::cin >> binary;
    if (binary < 0) { /// check if number is smaller then zero
        std::cerr << "Error 1: The number can not be smaller then zero!\n";
        return 1;
    } /// end if

    /// initialization of data
    int index = 1, decimal = 0, pow = 1, digit = 0;

    /// while not end of binary
    while ((digit = binary / index) != 0) {
        digit %= 10;
        /// get digit
        if (digit > 1) { /// check if digit is not one
            std::cerr << "Error 2: Digits must be 1 or 0!\n";
            return 2;
        } /// end if
        decimal += digit * pow;
        index *= 10;
        pow *= 2;
    } /// end while

    /// output number
    std::cout << binary << " = " << decimal << std::endl;
    return 0; /// program ends successfully
} /// end function main
