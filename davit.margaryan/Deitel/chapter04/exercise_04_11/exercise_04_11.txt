a)
    if(age >= 65) {
        std::cout << "Age greater or equal 65" << std::endl;
    } else {
        std::cout << "Age smaller 65" << std::endl;
    }

b)
    if(age >= 65) {
        std::cout << "Age greater or equal 65" << std::endl;
    } else {
        std::cout << "Age smaller 65" << std::endl;
    }

c)
    int x = 1, total = 0;
    while (x <= 10) {
        total += x;
        ++x;
    }

d)
    while (x <= 100) {
        total += x;
        ++x;
    }

e)
    int y = 100;
    while (y > 0) {
        std::cout << y << std::endl;
        --y;
    }
