///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    double number1 = 0, number2 = 0, 
        number3 =  0;

    std::cout << "Enter the sides of triangle: ";
    std::cin >> number1 >> number2 >> number3;

    if (number1 <= 0) {
        std::cerr << "Error 1: Invalid side.\n";
        return 1;
    }

    if (number2 <= 0) {
        std::cerr << "Error 1: Invalid side.\n";
        return 1;
    }

    if (number3 <= 0) {
        std::cerr << "Error 1: Invalid side.\n";
        return 1;
    }

    if (number1 + number2 <= number3) {
        std::cout << "It is not a triangle.\n";
    } else if (number2 + number3 <= number1) {
        std::cout << "It is not a triangle.\n";
    } else if (number3 + number1 <= number2) {
        std::cout << "It is not a triangle.\n";
    } else {
        std::cout << "It is a triangle.\n"; 
    }

    return 0; /// program ends successfully
} /// end function main
