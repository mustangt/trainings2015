#ifndef __INVOICE_HPP__
#define __INVOICE_HPP__
/// Invoice.hpp
/// definition of class Invoice

#include <string> /// includes c++ standard string class

class Invoice
{
public:
    Invoice(std:: string number, std::string description,
            int quantity, int price);
    void setNumber(std::string number); /// set number_ to number
    std::string getNumber(); /// get the number
    void setDescription(std::string description); /// set description_
    std::string getDescription(); ///get description_
    void setQuantity(int quantity); 
    int getQuantity();
    void setPrice(int price);
    int getPrice();
    int getInvoiceAmount();
private:
    std::string number_;
    std::string description_;
    int quantity_;
    int price_;
}; /// end class Invoice

#endif /// __INVOICE_HPP__
