/// GradeBook.cpp
/// Impliementations of the GradeBook class member-function definitions.

#include "GradeBook.hpp" /// include definition of class GradeBook
#include <iostream> /// allows program to perform input and output

/// gradebook constructor initializes object's data members
GradeBook::GradeBook(std::string courseName, std::string instructorName)
{
    setCourseName(courseName); /// validate and store courseName_
    setInstructorName(instructorName); /// validate and store instructorName_
    
} /// end construct

/// function setCurseName assign curseName
void
GradeBook::setCourseName(std::string courseName)
{
    if(courseName.size() <= 25) { /// if name size is currect
        courseName_ = courseName;
    } /// endif
    if(courseName.size() > 25) { /// if the name size grather then 25
        courseName_ = courseName.substr(0, 25); /// get 25 characters from name
        std::cout << "The size of instructor "
                  << "name can not be grather then 25!" << std::endl;
    } /// endif
} /// end function setCurseName

/// function getCurseName returns curseName
std::string
GradeBook::getCourseName()
{
    return courseName_;
} /// end function getCursName

/// function setInstructorName assign instructorName
void
GradeBook::setInstructorName(std::string instructorName)
{
    if(instructorName.size() <= 25) { /// if name size is currect
        instructorName_ = instructorName;
    } /// endif
    if(instructorName.size() > 25) { /// if the name size grather then 25
        instructorName_ = instructorName.substr(0, 25); /// get 25 characters 
        std::cout << "The size of instructor "
                  << "name can not be grather then 25!" << std::endl;
    } /// endif
} /// end function setInstructorName

/// function getInstructorNmae returns instructorNe
std::string
GradeBook::getInstructorName()
{
    return instructorName_;
} /// end function getInstructorNmae

/// function displayMessage prints message
void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for " 
              << getCourseName() << "!\n"
              << "The instructor is "
              << getInstructorName() << ".\n";
} /// end function displayMessage
