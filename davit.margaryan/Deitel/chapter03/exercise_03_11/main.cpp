/// Create object Gradebook and test

#include "GradeBook.hpp" /// allows program to perform gradebook
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    GradeBook gradeBook("John", "Tom"); /// crate Object gradeBook
    gradeBook.displayMessage(); /// print message
    
    /// test setters and getters
    gradeBook.setCourseName("John Smith from Canada !! test @#123456789" );
    /// print manually
    std::cout << "Course: " << gradeBook.getCourseName() << std::endl
              << "Instructor: " << gradeBook.getInstructorName() << std::endl;
    return 0; /// program ends successfully
} /// end function main
