#include <iostream>

int 
main()
{
    int counter = 1, number;
    
    std::cout << "Enter N: " ;
    std::cin  >> number ;

    if (number <= 0){
        std::cout << "Error 1: The number should be non-zero positive." << std::endl;
        return 1;
    }

    std::cout << "\n\tN\t10*N\t100*N\t1000*N\n\n"; 

    while(counter <= number){
        std::cout << "\t" << counter << "\t" << 10 * counter << "\t" << 100 * counter << "\t" << 1000 * counter << std::endl;
        counter++;
    }
    
    return 0;
}
