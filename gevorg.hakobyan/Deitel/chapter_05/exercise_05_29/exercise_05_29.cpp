#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate){
        long double principal = 24.00;

        std::cout << std::setw(15) << "Rate: " << rate << "%" << std::endl;
        std::cout << "\nYear" << std::setw(27) << "Amount on deposit" << std::endl;
        std::cout << std::fixed << std::setprecision(2);

        long double percentage = 1.0 + rate * 0.01;
        for (int year = 1; year <= 400; ++year){
            principal *= percentage;
            std::cout << std::setw(4) << year << std::setw(25) << principal << " $" << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}
