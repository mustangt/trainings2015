#include <iostream>
#include <string>

int
main()
{
    std::cout << "The Twelve Days of Christmas.\n" << std::endl;

    std::string next;
   
    for (int coupletNumber = 1; coupletNumber <= 12; ++coupletNumber) {
        std::string day, text1;

        switch (coupletNumber) { 
        case 1:  day = "First";    break;                    
        case 2:  day = "Second";   break;
        case 3:  day = "Third";    break;
        case 4:  day = "Fourth";   break;
        case 5:  day = "Fifth";    break;
        case 6:  day = "Sixth";    break;
        case 7:  day = "Seventh";  break;
        case 8:  day = "Eight";    break;
        case 9:  day = "Ninth";    break;
        case 10: day = "Tenth";    break;
        case 11: day = "Eleventh"; break;
        case 12: day = "Twelfth";  break;       
        }
        std::cout << day << std::endl;

        switch (coupletNumber) {     
        case 1:  text1 = "A Partridge in a Pear Tree";                break;
        case 2:  text1 = "Two Turtle Doves\nand ";                    break;
        case 3:  text1 = "Three French Hens\n";                       break;
        case 4:  text1 = "Four Calling Birds*\n";                     break;
        case 5:  text1 = "Five Golden Rings\n";                       break;
        case 6:  text1 = "Six Geese a Laying\n";                      break;
        case 7:  text1 = "Seven Swans a Swimming\n";                  break;
        case 8:  text1 = "Eight Maids a Milking\n";                   break;
        case 9:  text1 = "Nine Ladies Dancing\n";                     break;
        case 10: text1 = "Ten Lords a Leaping\n";                     break;
        case 11: text1 = "Eleven Pipers PipingTen Lords a Leaping\n"; break;
        case 12: text1 = "12 Drummers Drumming\n";                    break;
        }    
        std::cout << "\nOn the " << day << " day of Christmas\nmy true love sent to me:\n" << text1 << next << "\n" << std::endl;
        next = text1 + next;
    }

    return 0;
}
