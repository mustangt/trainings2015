a)  1.For (the loop "for" should be start in small letter)
    2.in the "for" loop we can't write comma "," , only ";", but comma we can write only after increment or decrement , when after that 
      will be operation (example. for (int i = 1; i <= 10; i++, s += i)
    3.this loop will be stopped when the memory "int" is full, to fix this problem we can change x++ into --x.
b)  1.without break after case 0: if I enter the even number, this file will display Even integer
                                                                                     Odd integer
c)  1.this loop will be stopped when the memory "int" is full, to fix this problem we can change x++ into --x.
d)  1.While (the loop "do...while" or "while" should be start in small letter)
    2.and if I want this program to display the numbers from 2 to 100 , I should write "while (counter <= 100)"
