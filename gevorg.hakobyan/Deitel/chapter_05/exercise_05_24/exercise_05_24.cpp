#include <iostream>
#include <cmath>

int
main()
{
    int number;

    std::cout << "Enter the number (1 - 19): ";
    std::cin  >> number;
    
    if (number < 1 || number > 19 || number % 2 == 0){
        std::cout << "Error 1: The number should be in the range of 1 - 19 and odd.\nTry again." << std::endl;
        return 1;
    }

    int center = number / 2 + 1;
    for (int horizontal = 1; horizontal <= number; ++horizontal){        
        int horizontalSpaceCount = std::abs(center - horizontal);
        for (int vertical = 1; vertical <= number; ++vertical){
            int verticalSpaceCount = std::abs(center - vertical);
            std::cout << (center > horizontalSpaceCount + verticalSpaceCount ? "*" : " ");
        }
        std::cout << std::endl;
    }

    return 0;
} 
