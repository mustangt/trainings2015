#include <iostream>

int
main()
{
    int a, b, c, d, e, min, max;
    std::cout << "Enter five numbers: ";
    std::cin >> a >> b >> c >> d >> e;
    min = a;
    max = b;
    if(b < min){
        min = b;
    }
    if(c < min){
        min = c;
    }
    if(d < min){
        min = d;
    }
    if(e < min){
        min = e;
    }
    std::cout << "Minimal value " << min << std::endl;

    if(b > max){
        max = b;
    }
    if(c > max){
        max = c;
    }
    if(d > max){
        max = d;
    }
    if(e > max){
        max = e;
    }
    std::cout << "Maximal value " << max << std::endl;

    return 0;
}
