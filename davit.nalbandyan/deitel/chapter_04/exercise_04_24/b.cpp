#include <iostream>

int
main()
{
    int x, y;
    std::cout << "\nInput y: ";
    std::cin >> y;
    std::cout << "\nInput x: ";
    std::cin >> x;
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }
    return 0;
}
