#include <iostream>

int
main()
{
    int number = 1;
    while (true) {
        number *= 2;
        std::cout << number << std::endl;
    }
    return 0;
}
