#include <iostream>

int 
main()
{
    int workHours = 0;
    while(-1 != workHours){
        std::cout << "\nEnter the number of working hours (-1 if the input is completed): ";
        std::cin >> workHours;
        if (-1 == workHours){
            return 0;
        }
        if (workHours < -1) {
            std::cout << "\nInput not valid, closing the program\n";
            return 1;
        }
        double rate;
        std::cout << "\nEnter the employee's hourly rate ($00.00): ";
        std::cin >> rate;
        if (rate < 0) {
            std::cout << "\nInput not valid, closing the program\n";
            return 2;
        }
        double payment = rate * workHours;
        if (workHours > 40){
            payment += rate * ((workHours - 40) / 2);
        }
        std::cout <<"\nPayment: " << payment;
    }
    return 0;
}
