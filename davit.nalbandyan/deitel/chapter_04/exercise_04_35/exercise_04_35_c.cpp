#include <iostream>

int
main()
{
    int n;
    std::cout << "Input your number: ";
    std::cin >> n;
    if (n < 0) {
        std::cout << "\nError1: Input not valid" << std::endl;
        return 1;
    } 
    double eulerX = 1;
    int i = 1, x = 1, factorial = 1;
    while (i <= n) {
        x *= n;
        factorial *= i;
        eulerX += x / factorial;
        ++i;
    }    
    std::cout << "\ne^x = " << eulerX << std::endl;
    return 0;
}
