#include <iostream>

int
main()
{
    int row = 1, length = 8;
    while (row <= length) {
        if (row % 2 == 0) {
            std::cout << ' ';
        }
        int column = 1;
        while (column <= length) {
             std::cout << "* ";
             ++column;
        }    
        ++row;
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}
