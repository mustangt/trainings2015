#include <iostream>

int
main()
{
    int counter = 2;
    do {
        counter += 2;
    } while (counter <= 100);
    return 0;
}
