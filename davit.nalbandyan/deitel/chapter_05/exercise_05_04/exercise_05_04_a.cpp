#include <iostream>

int
main()
{
    for (int x = 100; x >= 1; x--) {
        std::cout << x << std::endl;
    }
    return 0;
}
