#include <iostream>
#include <iomanip>

int
main()
{
    double sales;
    std::cout << "Enter sales volume in dollar (-1, if the input is completed): ";
    std::cin >> sales; 
    while (sales != -1) {
        if (sales < 0) {
            std::cout << "Invalid sales volume. Try again!" << std::endl;
            return 1;
        }
        double earnings = 0.09 * sales + 200;
        std::cout << "Еarnings: $ " << std::setprecision(2) << std::fixed << earnings << std::endl;
        std::cout << "\n\nEnter sales volume in dollar (-1, if the input is completed): ";
        std::cin >> sales;
    }

    return 0;
}
