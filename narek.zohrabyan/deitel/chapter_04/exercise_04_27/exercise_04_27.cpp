#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter binary number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Please enter binary positive number (only zeroes and ones). Try again!" << std::endl;
        return 1;
    }
    int decimal = 0, degree = 1;
    while (number > 0) {
        int digit = number % 10;
        if (digit > 1) {
            std::cout << "Error 2: This is not binary number. Try Again!" << std::endl;
            return 2;
        }
        decimal += digit * degree;
        number /= 10;
        degree *= 2;
    }
    std::cout << decimal << std::endl;
    return 0;
}
