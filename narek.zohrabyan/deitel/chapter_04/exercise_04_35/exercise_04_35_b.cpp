#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Invalid number. Try again!" << std::endl;
        return 1;
    }
    int counter = 1, factorial = 1;
    double eulerNumber = 1; 
    while (counter <= number) {
        factorial *= counter;
        eulerNumber += 1.0 / factorial;
        ++counter;
    }
    std::cout << "e = " << eulerNumber << std::endl;   
    return 0;
}
