#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(500);
    Account account2(100);

    std::cout << "Initial balance account1: " << account1.getBalance() << std::endl;
    std::cout << "Initial balance account2: " << account2.getBalance() << std::endl;
    
    account1.debit(200);
    account2.debit(200);
    std::cout << "Balance account1 after debit: " << account1.getBalance() << std::endl;
    std::cout << "Balance account2 after debit: " << account2.getBalance() << std::endl;

    account1.credit(500);
    account2.credit(500);
    std::cout << "Balance account1 after credit: " << account1.getBalance() << std::endl;
    std::cout << "Balance account2 after credit: " << account2.getBalance() << std::endl;

    return 0;
}
