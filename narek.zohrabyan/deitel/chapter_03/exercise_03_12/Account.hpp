class Account
{
public:
   Account(int balance);
   void debit(int debit);
   void credit(int credit);
   int getBalance();
  
private:
   int balance_;
};
