#include <string>

class Invoice
{
public:
    Invoice(std::string article, std::string description, int quantity, int price);

    void setArticle(std::string article);
    std::string getArticle();

    void setDescription(std::string description);
    std::string getDescription();

    void setQuantity(int quantity);
    int getQuantity();

    void setPrice(int price);
    int getPrice();

    int getInvoiceAmount();

private:
    std::string article_;
    std::string description_;
    int quantity_;
    int price_;

};
