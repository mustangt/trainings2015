#include "Date.hpp"
#include <iostream>

Date::Date(int month, int day, int year)
{
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
Date::setMonth(int month)
{
    if (month < 1) {
        month_ = 1;
        std::cout << "Invlaid month!. Reset month to 1." << std::endl;
    }
    if (month > 12) {
        month_ = 1;
        std::cout << "Invalid month!. Reset month to 1." << std::endl;
    }
    if (month >= 1) {
        if(month <= 12) {
            month_ = month;
        }                   
    }
}

int
Date::getMonth()
{
    return month_;
}

void
Date::setDay(int day)
{
    day_ = day;
}

int
Date::getDay()
{
    return day_;
}

void
Date::setYear(int year)
{
    year_ = year;
}

int
Date::getYear()
{
    return year_;
}

void
Date::displayDate()
{
    std::cout << getMonth() << "/" << getDay() << "/" << getYear() << std::endl;
}
