#include <iostream>
#include <iomanip>

int
main() 
{   
    std::cout << "(a)" << std::endl;
    for (int i = 1; i <= 10; ++i){
        for (int j = 1; j <= 10; ++j) {
            std::cout << (j <= i ? "*" : " ");
        }
        std::cout << std::endl;
    }
    std::cout << "\n(b)" << std::endl;
    for (int i = 1; i <= 10; ++i){
        for (int j = 10; j >= 1; --j) {
            std::cout << (j >= i ? "*" : " ");
        }
        std::cout << std::endl;
    }
    std::cout << "\n(c)" << std::endl;
    for (int i = 1; i <= 10; ++i){
        for (int j = 1; j <= 11; ++j) {
            std::cout << (j <= i ? " " : "*");
        }
        std::cout << std::endl;
    }
    std::cout << "\n(d)" << std::endl;
    for (int i = 1; i <= 10; ++i){
        for (int j = 10; j >= 0; --j) {
            std::cout << (j >= i ? " " : "*");
        }
        std::cout <<std::endl;
    }

    std::cout << "\n(a)" << std::setw(12) << "(b)" << std::setw(13) << "(c)" << std::setw(14) << "(d)" << std::endl;
    for (int i = 1; i <= 10; ++i) {
        for (int j = 1; j <= 10; ++j) {
            std::cout << (j <= i ? "*" : " ");
        }
        std::cout << " ";
        for (int j = 10; j >= 1; --j) {
            std::cout << (j >= i ? "*" : " ");
        }
        std::cout << " ";
        for (int j = 1; j <= 11; ++j) {
            std::cout << (j <= i ? " " : "*");
        }
        std::cout << " ";
        for (int j = 10; j >= 0; --j) {
            std::cout << (j >= i ? " " : "*");
        }
        std::cout << std::endl;
    }
    return 0;
}
