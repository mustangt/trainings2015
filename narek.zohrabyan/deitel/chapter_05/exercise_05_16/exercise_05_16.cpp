#include <iostream>
#include <iomanip>

int
main()
{
    int amount = 100000;
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    for (int year = 1; year <= 10; ++year) { 
        amount = amount * 105 / 100;
        std::cout << std::setw(4) << year << std::setw(21) << amount / 100 << "." << amount % 100 << std::endl;
    }
    return 0;
}
