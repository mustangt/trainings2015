#include <iostream>

int
main()
{
    int quantity, sum = 0;
    std::cout << "Enter quantity: ";
    std::cin >> quantity;
    for (int counter = 0; counter < quantity; ++counter) {
        int number;
        std::cout << "Enter the number: ";
        std::cin >> number;
        sum += number;
    }
    std::cout << "Sum of numbers = " << sum << std::endl;
    return 0;
}
