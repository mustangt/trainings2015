#include <iostream>

int
main()
{
    for (int i = 0; i < 5; ++i) {
        int number;
        std::cout << "Enter number from 1 to 30: ";
        std::cin >> number;
        if ((number < 1) || (number > 30)) {
            std::cout << "Error 1: Invalid number. Try again!" << std::endl;
            return 1;
        }
        for (int counter = 0; counter < number; ++counter) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}
