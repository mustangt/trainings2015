#include <iostream>

int
main()
{
    int factorial = 1;
    for(int number = 1; number <= 5; ++number) { 
        factorial *= number;
        std::cout << number << "! = " << factorial << std::endl; /// We can't calculate a factorial 20 because range int is too small.
    }
    return 0;
}
