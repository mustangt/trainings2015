#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::setw(5) << "Number" << std::setw(10) << "Binary" << std::setw(15) << "Octal" << std::setw(20) << "Hexadecimal" << std::endl;
    for (int decimal = 1; decimal <= 256; ++decimal) {
        std::cout << std::dec << decimal << std::setw(10);
        int binary = decimal, counter = 256;
        while (counter >= 1) {
            if (binary >= counter) {
                std::cout << "1";
                binary -= counter;
            } else {
                std::cout << "0";
            }
            counter /= 2;
        }         
        std::cout << std::setw(12) << std::oct << decimal << std::setw(15) << std::hex << decimal << std::endl; 
    }
    return 0;
}
