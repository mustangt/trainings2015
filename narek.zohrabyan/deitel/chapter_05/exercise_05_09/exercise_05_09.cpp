#include <iostream>

int
main()
{
    int sum = 1;
    for (int number = 1; number <= 15; number += 2) {
            std::cout << number << std::endl;
            sum *= number;
    }
    std::cout << "Multiplication odd whole 1 to 15 is " << sum << std::endl;
    return 0;
}
