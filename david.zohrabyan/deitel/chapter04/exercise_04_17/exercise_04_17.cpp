#include <iostream>

int
main()                                                                        
{
    double counter = 1, largest = 0;
    while (counter <= 10)
    {
        double number;
        std::cout << "Enter positive number: ";
        std::cin >> number;
        if(number < 0) {
            std::cout << "Error 1: The entered number is not positive. " << std::endl;
            return 1;
        } else if(number > largest) { 
            largest = number;
        }
        counter++;
    }
    
    std::cout << "Largest number is " << largest << std::endl;
   
    return 0;
}
 
