#include <iostream>

int
main()
{
    { //Exercise_05_22-a
        int x, y;
        std::cout << "Enter: ";
        std::cin >> x >> y;
        if ((!(x < 5) && !(y >= 7)) == !((x < 5) || (y >= 7))) {
            std::cout << "(!(x < 5) && !(y >= 7)) and (!((x < 5) || (y >= 7))) Are Equal!" << std::endl;
        } else {
            std::cout << "(!(x < 5) && !(y >= 7)) and (!((x < 5) || (y >= 7))) Aren`t Equal!" << std::endl;
        }
    }

    { //Exercise_05_22-b
        int a, b, g;
        std::cout << "Enter: ";
        std::cin >> a >> b >> g;
        if ((!(a == b) || !(g != 5)) == (!((a == b) && (g != 5)))) {
            std::cout << "(!(a == b) || !(g != 5)) and (!((a == b) && (g != 5)) Are Equal!" << std::endl;
        } else {
            std::cout << "(!(a == b) || !(g != 5) and if (!((a == b) && (g != 5)) Aren`t Equal!" << std::endl;
        }
    }

    { //Exercise_05_22-c
        int x, y;
        std::cout << "Enter: ";
        std::cin >> x >> y;
        if (!((x <= 8) && (y > 4)) == ((x > 8) || (y <= 4))) {
            std::cout << "!((x <= 8) && (y > 4)) and ((x > 8) || (y <= 4)) Are Equal!" << std::endl;
        } else {
            std::cout << "!((x <= 8) && (y > 4)) and ((x > 8) || (y <= 4))) Aren`t Equal!" << std::endl;
        }  
    }

    { //Exercise_05_22-d
        int i, j;
        std::cout << "Enter: ";
        std::cin >> i >> j;
        if (!((i > 4) || (j <= 6)) == ((i <= 4) && (j > 6))) {
            std::cout << "(!((i > 4) || (j <= 6)) and ((i <= 4) && (j > 6)) Are Equal!" << std::endl;
        } else {
            std::cout << "(!((i > 4) || (j <= 6)) and ((i <= 4) && (j > 6)) Aren`t Equal!" << std::endl;
        }
    }

    return 0;
}    
