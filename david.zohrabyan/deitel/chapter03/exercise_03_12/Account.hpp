/// The  heading file of Account
class Account
{
public:
    ///Constructor initializing  
    Account(int);  
    
    /// The function to add the specified sum in the current balance
    void credit(int credit);
    
    /// The function draws money from account
    void debit(int debit);
    
    /// The function returns the current balance
    int getBalance();
    
private:    
    /// The current balance of the account 
    int balance_;
};
