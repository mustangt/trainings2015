/// This program prints the sum,work a difference and private of two numbers
#include <iostream> /// standard input and output
 
/// function main begins program execution
int
main()
{
    /// announcement of variables
    int a,b,c;

    std::cout << "Enter variable a: " << std::endl; /// prompt user for data
    std::cin >> a; ///read integer from user into "a"

    std::cout << "Enter variable b: " << std::endl;/// prompt user for data
    std::cin >> b; /// read integer from user into "b"

    c = a + b ; /// add the variables;store result in "c"
    std::cout << "a+b=" << c << std::endl; /// display "c";end line

    c = a * b ; /// multiplicate the variables;store result in "c"
    std::cout << "a*b=" << c << std::endl; /// display "c";end line

    c = a - b ; /// sub the variables;store result in "c"
    std::cout << "a-b= " << c << std::endl; /// display "c";end line

    c = a / b ; /// div the variables;store result in "c"
    std::cout << "a/b= " << c << std::endl; /// display "c";end line
 
    return 0; /// program ended successfully  
} ///end function main
